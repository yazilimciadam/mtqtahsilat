FROM php:7.4-apache
RUN docker-php-ext-install mysqli
RUN docker-php-ext-enable mysqli
RUN a2enmod rewrite
RUN a2dismod proxy
RUN a2enmod proxy proxy_fcgi