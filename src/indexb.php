<?php
session_start();
ob_start();
include('layout/header.php');

include('db/mysql_crud.php');
$db = new Database();
$db->connect();

$db->select('yukler', '*', null, null, "yuk_id DESC");
$res = $db->getResult();

$db->select("araclar", "*", null, null, "arac_id DESC");
$araclar = $db->getResult();
$db->select("yuk_durumlari", "*", null, null, "yuk_durum_id DESC");
$durumlar = $db->getResult();

if ($_POST["arac_guncelle"]) {
    $db->update("yukler",  array("durum"=>$_POST["durum_id"]), "yuk_id=".$_POST["arac_id"]);
    var_dump($db->getResult());
   

}

?>

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
    <h1 class="h2">Dashboard</h1>
    <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
            <button class="btn btn-sm btn-outline-secondary" type="button" id="myModal"  data-toggle="modal" data-target="#exampleModal">Yük Durum Güncelle</button>

        </div>

    </div>
</div>

<div class="row">

    <h3> Ödeme Bekleyen Yükler</h3>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Yük</th>
                <th scope="col">Rota</th>
                <th scope="col">Araç</th>
                <th scope="col">Durum</th>
               
            </tr>
        </thead>
        <tbody>
            <?php for ($i=0; $i < count($res); $i++) { 
                $element = $res[$i];
                if ($element["durum"] != 1) {
                    # code...
                }
           ?>
            <tr>
                <th scope="row">1</th>
                <td> <?php echo $element["yük"] ?> </td>
                <td> <?php echo $element["rota"] ?> </td>
                <td>
            <?php for ($j = 0; $j < count($araclar); $j++) {
              $arac = $araclar[$j];
              if ($arac["arac_id"] == $element["arac"]) {
                echo $arac["plaka"];
              }
            } ?>
          </td>
          <td>
            <?php for ($j = 0; $j < count($durumlar); $j++) {
              $element32 = $durumlar[$j];
              if ($element32["yuk_durum_id"] == $element["durum"]) {
                echo $element32["durum"];
              }
            } ?>
          </td>
            </tr>
       <?php  } ?>
        </tbody>
    </table>





</div>


<div class="row">

    <h3> Boşta Bekleyen Araçlar</h3>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Araç</th>
                <th scope="col">Rota</th>
                <th scope="col">Araç</th>
                <th scope="col">Durum</th>
               
            </tr>
        </thead>
        <tbody>
            <?php for ($i=0; $i < count($araclar); $i++) { 
                $element = $araclar[$i];
              
           ?>
            <tr>
                <th scope="row">1</th>
                <td> <?php echo $element["yük"] ?> </td>
                <td> <?php echo $element["rota"] ?> </td>
                
          <td>
            <?php for ($j = 0; $j < count($durumlar); $j++) {
              $element32 = $durumlar[$j];
              if ($element32["yuk_durum_id"] == $element["durum"]) {
                echo $element32["durum"];
              }
            } ?>
          </td>
            </tr>
       <?php  } ?>
        </tbody>
    </table>





</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form action="index.php" method="post">
        <input type="hidden" name="arac_guncelle" value="1">
        <select name="arac_id" class="form-control mb-2">
            <?php for ($i=0; $i < count($res); $i++) { 
                $element = $res[$i];
                echo '<option value="'.$element["arac"].'">'.$element["yük"]."</option>";
            } ?>
        </select>
        <select name="durum_id" class="form-control mb-2">
            <?php for ($i=0; $i < count($durumlar); $i++) { 
                $element = $durumlar[$i];
                echo '<option value="'.$element["yuk_durum_id"].'">'.$element["durum"]."</option>";
            } ?>
        </select>
    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" id="close" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
<script>
$('#myModal').on('click', function(){
            $('#exampleModal').modal('show');
        });
$('#close').on('click', function(){
            $('#exampleModal').modal('hide');
        });
</script>

<?php include('layout/footer.php'); ?>