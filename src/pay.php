<?php

include('db/mysql_crud.php');
$db = new Database();
$db->connect();

if ($_POST["fat_id"]) {
    $db->select("faturalar", "*", null, 'vergi=' . $_POST["vergi"] . ' AND fat_id="' . $_POST["fat_id"] . '"', null);
    $res = $db->getResult();
} else {
    echo  '<script>alert("Lütfen bir fatura seçiniz");</script>';
    header("Refresh:0; url=index.php");
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>MTQ Advertising Agency - Ödeme</title>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css'>
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.8.1/css/all.css'>
    <link rel="stylesheet" href="./style.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
    <script src="/script.js"></script>

</head>

<body>
    <!-- partial:index.partial.html -->
    <div class="container py-5">
        <!-- For demo purpose -->
        <div class="row mb-4">
            <div class="col-lg-8 mx-auto text-center">
                <h1 class="display-4">Ödeme Formu</h1>
            </div>
        </div> <!-- End -->
        <div class="row">
            <div class="col-lg-6 mx-auto">
                <div class="card ">
                    <div class="card-header">
                        <div class="bg-white shadow-sm pt-4 pl-2 pr-2 pb-2">
                            <!-- Credit card form tabs -->

                            <div class="alert alert-primary" role="alert">
                            <strong>Sayın: </strong><?php echo $res[0]["firma_unvan"]?>
</div>
<div class="alert alert-primary" role="alert">
                            <strong>Ödeme Tutarı (KDV Hariç): </strong><?php echo $res[0]["fatura_miktar"]?><br>
                            <strong>Ödeme Tutarı (KDV Dahil): </strong><?php echo $res[0]["fatura_miktar"]+($res[0]["fatura_miktar"]*0.18)?>
</div>
<div class="alert alert-danger" role="alert">
                            <strong> <?php $res[0]["fat_id"] ?> Nolu Faturanızın Son Ödeme Tarihi: </strong><?php echo $res[0]["son_odeme"]?>
</div>

                            <ul role="tablist" class="nav bg-light nav-pills rounded nav-fill mb-3">
                                <li class="nav-item"> <a data-toggle="pill" href="#credit-card" class="nav-link active "> <i class="fas fa-credit-card mr-2"></i> Credit Card </a> </li>
                                <li class="nav-item"> <a data-toggle="pill" href="#havale" class="nav-link "> <i class="fas fa-receipt"></i></i> Havale/EFT </a> </li>

                            </ul>
                        </div> <!-- End -->
                        <!-- Credit card form content -->
                        <div class="tab-content">
                            <!-- credit card info-->
                            <div id="credit-card" class="tab-pane fade show active pt-3">
                                <form role="form">
                                    <div class="form-group"> <label for="username">
                                            <h6>Kart Sahibi</h6>
                                        </label> <input type="text" name="username" placeholder="Erol Avcı" required class="form-control "> </div>
                                    <div class="form-group"> <label for="cardNumber">
                                            <h6>Kart Numarası</h6>
                                        </label>
                                        <div class="input-group"> <input type="text" name="cardNumber" placeholder="1234 4444 4444 5555" class="form-control " required>
                                            <div class="input-group-append"> <span class="input-group-text text-muted"> <i class="fab fa-cc-visa mx-1"></i> <i class="fab fa-cc-mastercard mx-1"></i> <i class="fab fa-cc-amex mx-1"></i> </span> </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <div class="form-group"> <label><span class="hidden-xs">
                                                        <h6>Son Kullanma Tarihi</h6>
                                                    </span></label>
                                                <div class="input-group"> <input type="number" placeholder="MM" name="" class="form-control" required> <input type="number" placeholder="YY" name="" class="form-control" required> </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group mb-4"> <label data-toggle="tooltip" title="Three digit CV code on the back of your card">
                                                    <h6>CVV <i class="fa fa-question-circle d-inline"></i></h6>
                                                </label> <input type="text" required class="form-control"> </div>
                                        </div>
                                    </div>
                                    <div class="card-footer"> <button type="button" class="subscribe btn btn-primary btn-block shadow-sm"> Confirm Payment </button>
                                </form>
                            </div>
                        </div> <!-- End -->
                        <!-- Paypal info -->
                        <div id="havale" class="tab-pane fade pt-3">
                        <div class="alert alert-success" role="alert">
  <h4 class="alert-heading">Havale/EFT</h4>
  <p>Lütfen ödeme sonrası bizimle iletişime geçiniz ve dekontu info@mtqcorporation.com Adresine mail atınız.</p>
  <hr>
  <p class="mb-0"> TR77 0011 1000 0000 0109 1853 29 - QNB Finansabank</p>
  <p class="mb-0"> TR54 0004 6007 5088 8000 1183 23 - Akbank</p>
  <p class="mb-0"> TR95 0006 2001 6770 0006 2997 97 - Garanti BBVA</p>
</div>
                        </div> <!-- End -->
                        <!-- bank transfer info -->

                        <!-- End -->
                    </div>
                </div>
            </div>
        </div>
        <!-- partial -->

</body>


</html>