

<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>MTQ Advertising Agency - Tahsilat</title>
  <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css'>
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.8.1/css/all.css'><link rel="stylesheet" href="./style.css">
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script><script  src="./script.js"></script>

</head>
<body>
<!-- partial:index.partial.html -->
<div class="container py-5">
    <!-- For demo purpose -->
    <div class="row mb-4">
        <div class="col-lg-8 mx-auto text-center">
            <h1 class="display-4">MTQ Advertising Agency            </h1>
            <h3>Tahsilat Ekranı</h3>
        </div>
    </div> <!-- End -->
    <div class="row">
        <div class="col-lg-6 mx-auto">
            <div class="card ">
                <div class="card-header">
               
                    <!-- Credit card form content -->
                    <div class="tab-content">
                        <!-- credit card info-->
                        <div id="credit-card" class="tab-pane fade show active pt-3">
                            <form role="form" method="POST" action="pay.php">
                                <div class="form-group"> <label for="username">
                                        <h6>Vergi No:</h6>
                                    </label> <input type="text" name="vergi" placeholder="Vergi No:" required class="form-control "> </div>
                                <div class="form-group"> <label for="cardNumber">
                                        <h6>Fatura No:</h6>
                                    </label>
                                    <div class="input-group"> <input type="text" name="fat_id" placeholder="Fatura No:" class="form-control " required>
                                       
                                    </div>
                                </div>
                          
                                <div class="card-footer"> <button type="submit" class="subscribe btn btn-primary btn-block shadow-sm"> Cari Hesap Sorgula </button>
                            </form>
                        </div>
                    </div> <!-- End -->
                 
                    <!-- End -->
                </div>
            </div>
        </div>
    </div>
<!-- partial -->

</body>
</html>
